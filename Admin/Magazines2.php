<?php
   include('validatelogin.php');
  include('header.php');
  include('../config/connect.php');
 ?>
Magazines<!doctype html>
   <div class="app-main__outer">
                        <div class="app-main__inner">
                            <div class="app-page-title">
                                <div class="page-title-wrapper">
                                    <div class="page-title-heading">
                                        <div class="page-title-icon">
                                            <i class="pe-7s-display2 icon-gradient bg-mean-fruit">
                                            </i>
                                        </div>
                                        <div>Magazines
                                            <div class="page-title-subheading">
                                            	All students magazines here.
                                            </div>
                                        </div>
                                    </div>
           </div>
       </div>
             
       <div class="row">
                            <div class="col-md-12">
                                <div class="main-card mb-3 card">
                                    
 

   <div class='table-responsive'>
                                        <table class=' table table-borderless table-striped table-hover'>
                                            <thead class="text-center">
                                            <tr>
                                                <th class=''>~</th>
                                                <th>Magazine Title</th>
                                                <th class='text-center'>Number of Contributions</th>
                                                <th class='text-center'>Submitted date</th>
                                                <th class='text-center'>Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody class="text-center">
    <?php 
    $query=mysqli_query($connect,"SELECT m.magazine_id,m.name, COUNT(a.article_id) as total_articles,m.academic_year FROM `magazines` m, articles a WHERE m.magazine_id=a.magazine_id and a.is_approved=1 GROUP BY m.magazine_id ");

    while($row = mysqli_fetch_assoc($query)):

     ?>
    <tr>
      <th scope="row"></th>
      <td><?php echo $row['name'] ?></td>
      <td><?php echo $row['total_articles'] ?></td>
      <td><?php echo $row['academic_year'] ?></td>
            <td><a href="magazines.php?magazine_id=<?php echo $row['magazine_id']?>">View More</a></td>

    </tr>
    <?php endwhile; ?>
  </tbody></table>
                                    </div>  
                                    
                                </div>
                            </div>
                        </div>
                    </form>
                            <br><br><br>
                    <div class="app-wrapper-footer">
                        <div class="app-footer">
                            <div class="app-footer__inner">
                               
                                   <p>© 2020 KMD University, Incorporated</p>
                                
                            </div>
                        </div>
                    </div>    </div>
                <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        </div>
    </div>
<script type="text/javascript" src="./assets/scripts/main.js"></script></body>
</html>
